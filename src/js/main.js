$(document).ready(function(){

    var dropdownSelected,

        // Variable that stores google map
        map,

        // Data for markers: Name, Lat, Long, and zIndex for the order in which
        // they will be displayed
        radnje = [
            ['Srbija 1', 44.0115177, 20.9256058, 18],
            ['Srbija 2', 43.889404, 20.3264233, 17],
            ['Srbija 3', 43.4570425, 21.8964538, 16],
            ['Srbija 4', 43.8998716, 22.271009, 15],
            ['Nemacka 1', 48.7183525, 9.0147239, 14],
            ['Nemacka 2', 48.1550547, 11.4017536, 13],
            ['Srbija 5', 43.5718166, 21.3073555, 12],
            ['Nemacka 3', 49.436009, 10.9584137, 11],
            ['Nemacka 4', 48.7535159, 11.2395696, 10],
            ['Nemacka 5', 50.1213479,8.4964819, 9],
            ['Srbija 6', 44.8154033, 20.2825145, 8],
            ['Nemacka 6', 44.0115177, 20.9256058, 7],
            ['Nemacka 7', 44.0115177, 20.9256058, 6],
            ['Srbija 7', 44.9017787, 20.2480813, 5],
            ['Srbija 8', 44.8700015, 20.6290396, 4],
            ['Srbija 9', 44.2182476,22.4990014, 3],
            ['Srbija 10', 44.3079382, 20.5442832, 2],
            ['Srbija 11', 45.2715508, 19.7794015, 1]
        ];

    // Click and Set functions for the header dropdown
    $('.dropdown').on('click.dropDownMenu', function () {
        var dropdownContent = $('.dropdown-content');
        dropdownContent.toggle();
    });

    // Store dropdown selected item in a var and display it in the header
    $('.dropdown-content a').on('click.dropDownItem', function (){
        dropdownSelected = $(this);
        $('.dropbtn').text(dropdownSelected.text());
        $('.dropdown-content').hide();
    });


    // Setup event handler for clicking on the filter buttons
    $('.filterbtn').on('click.filter', function(){
        doFiltering($(this));
    });

    function doFiltering (sender){
        // this function should change the state of the clicked button
        // with ie. class toggling
        // and filter the elements from the table
        sender.toggleClass('redbtn').toggleClass('greybtn');
        //console.log(sender);
    };

    // Init and setup the Slider for the Page 1 top section
    $('.slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    }); // end slider init

    // Google maps set-up the map
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: {lat: 44.0115177, lng: 20.9256058},
            disableDefaultUI: true
        });

        setMarkers(map);
        }

    function setMarkers(map) {
        // Adds markers to the map.

        // Marker sizes are expressed as a Size of X,Y where the origin of the image
        // (0,0) is located in the top left of the image.

        // Origins, anchor positions and coordinates of the marker increase in the X
        // direction to the right and in the Y direction down.
        var image = {
            url: '../img/HireTest-logo.png',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(64, 64),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(30, 55)
        };
        // Shapes define the clickable region of the icon. The type defines an HTML
        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
        // The final coordinate closes the poly by connecting to the first coordinate.
        var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };
        for (var i = 0; i < radnje.length; i++) {
            var radnja = radnje[i];
            var marker = new google.maps.Marker({
                position: {lat: radnja[1], lng: radnja[2]},
                map: map,
                icon: image,
                shape: shape,
                title: radnja[0],
                zIndex: radnja[3]
            });
        }
    }

    initMap();

});
