var fs = require('fs');
var path = require('path');

var gulp = require('gulp');

// Sass documentation generation in src/doc folder
var sassdoc = require('sassdoc');

// Load Browser Sync to automatically serve & reload from dist folder
var browserSync = require('browser-sync').create();

// Load all gulp plugins automatically
// and attach them to the `plugins` object
var plugins = require('gulp-load-plugins')();

// Temporary solution until gulp 4
// https://github.com/gulpjs/gulp/issues/355
var runSequence = require('run-sequence');

var pkg = require('./package.json');
var dirs = pkg['h5bp-configs'].directories;

// ---------------------------------------------------------------------
// | Helper tasks                                                      |
// ---------------------------------------------------------------------

gulp.task('archive:create_archive_dir', function () {
    fs.mkdirSync(path.resolve(dirs.archive), '0755');
});

gulp.task('archive:zip', function (done) {

    var archiveName = path.resolve(dirs.archive, pkg.name + '_v' + pkg.version + '.zip');
    var archiver = require('archiver')('zip');
    var files = require('glob').sync('**/*.*', {
        'cwd': dirs.dist,
        'dot': true // include hidden files
    });
    var output = fs.createWriteStream(archiveName);

    archiver.on('error', function (error) {
        done();
        throw error;
    });

    output.on('close', done);

    files.forEach(function (file) {

        var filePath = path.resolve(dirs.dist, file);

        // `archiver.bulk` does not maintain the file
        // permissions, so we need to add files individually
        archiver.append(fs.createReadStream(filePath), {
            'name': file,
            'mode': fs.statSync(filePath).mode
        });

    });

    archiver.pipe(output);
    archiver.finalize();

});

gulp.task('clean', function (done) {
    require('del')([
        dirs.archive,
        dirs.dist
    ]).then(function () {
        done();
    });
});

gulp.task('copy', [
    'copy:.htaccess',
    'copy:page1.html',
    'copy:page2.html',
    'copy:page3.html',
    'copy:jquery',
    'copy:license',
    'copy:main.css',
    'copy:mystyle.css',
    'copy:misc',
    'copy:normalize'
]);

gulp.task('copy:.htaccess', function () {
    return gulp.src('node_modules/apache-server-configs/dist/.htaccess')
               .pipe(plugins.replace(/# ErrorDocument/g, 'ErrorDocument'))
               .pipe(gulp.dest(dirs.dist));
});

gulp.task('copy:page1.html', function () {
    return gulp.src(dirs.src + '/page1.html')
        .pipe(plugins.replace(/{{JQUERY_VERSION}}/g, pkg.devDependencies.jquery))
        .pipe(gulp.dest(dirs.dist));
});

gulp.task('copy:page2.html', function () {
    return gulp.src(dirs.src + '/page2.html')
        .pipe(plugins.replace(/{{JQUERY_VERSION}}/g, pkg.devDependencies.jquery))
        .pipe(gulp.dest(dirs.dist));
});

gulp.task('copy:page3.html', function () {
    return gulp.src(dirs.src + '/page3.html')
        .pipe(plugins.replace(/{{JQUERY_VERSION}}/g, pkg.devDependencies.jquery))
        .pipe(gulp.dest(dirs.dist));
});

gulp.task('copy:jquery', function () {
    return gulp.src(['node_modules/jquery/dist/jquery.min.js'])
               .pipe(plugins.rename('jquery-' + pkg.devDependencies.jquery + '.min.js'))
               .pipe(gulp.dest(dirs.dist + '/js/vendor'));
});

gulp.task('copy:license', function () {
    return gulp.src('LICENSE.txt')
               .pipe(gulp.dest(dirs.dist));
});

gulp.task('copy:main.css', function () {

    var banner = '/*! HTML5 Boilerplate v' + pkg.version +
                    ' | ' + pkg.license.type + ' License' +
                    ' | ' + pkg.homepage + ' */\n\n';

    return gulp.src(dirs.src + '/css/main.css')
               .pipe(plugins.header(banner))
               .pipe(plugins.autoprefixer({
                   browsers: ['last 2 versions', 'ie >= 8', '> 1%'],
                   cascade: false
               }))
               .pipe(gulp.dest(dirs.dist + '/css'));
});

gulp.task('copy:mystyle.css', function () {
    return gulp.src(dirs.src + '/css/mystyle.*')
        .pipe(gulp.dest(dirs.dist + '/css'));
});

gulp.task('copy:normalize', function () {
    return gulp.src('node_modules/normalize.css/normalize.css')
        .pipe(gulp.dest(dirs.dist + '/css'));
});

gulp.task('copy:misc', function () {
    return gulp.src([

        // Copy all files
        dirs.src + '/**/*',

        // Exclude the following files
        // (other tasks will handle the copying of these files)
        '!' + dirs.src + '/css/**',
        '!' + dirs.src + '/sass/**',
        '!' + dirs.src + '/doc/**',
        '!' + dirs.src + '/page1.html',
        '!' + dirs.src + '/page2.html',
        '!' + dirs.src + '/page3.html',
        '!' + dirs.src + '.editorconfig',
        '!' + dirs.src + '.gitatributes',
        '!' + dirs.src + '.gitignore'
    ], {

        // Include hidden files by default
        dot: true,
        // Exclude empty folders
        nodir: true

    }).pipe(gulp.dest(dirs.dist));
});

gulp.task('lint:js', function () {
    return gulp.src([
        'gulpfile.js',
        dirs.src + '/js/*.js',
        dirs.test + '/*.js'
    ]).pipe(plugins.jscs())
      // .pipe(plugins.jshint())
      // .pipe(plugins.jshint.reporter('jshint-stylish'))
      // .pipe(plugins.jshint.reporter('fail'));
});

gulp.task('sass', function(){
    return gulp.src(dirs.src + '/sass/mystyle.sass')
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass({
            errLogToConsole: true,
            outputStyle: 'nested'
        }).on('error', plugins.sass.logError))
        .pipe(plugins.sourcemaps.write())
        .pipe(plugins.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 8', '> 1%'],
            cascade: false
        }))
        .pipe(gulp.dest(dirs.src + '/css'));
});

gulp.task('sassdoc', function(){
    return gulp.src(dirs.src + '/sass/mystyle.sass')
        .pipe(sassdoc({
            dest: dirs.src + '/doc/sassdoc'
        }))
        .resume();
});


// ---------------------------------------------------------------------
// | Main tasks                                                        |
// ---------------------------------------------------------------------

gulp.task('archive', function (done) {
    runSequence(
        'build',
        'archive:create_archive_dir',
        'archive:zip',
    done);
});

gulp.task('build', function (done) {
    runSequence(
        ['clean', 'lint:js', 'sass', 'sassdoc'],
        'copy',
    done);
});

gulp.task('working', function (done) {
    runSequence(
        ['lint:js', 'sass'],
        'copy',
    done);
});

// Init Browser-Sync server from /dist
// Watch for changes in /scr for compiling and /dist for reloading
gulp.task('work', ['working'], function() {
    browserSync.init({
        server: {
            baseDir: dirs.dist,
            directory: true
        }
    });

    gulp.watch(dirs.src + '/**/*', ['working'])
        .on('change', function(event) {
             console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
         });
    gulp.watch(dirs.dist + "/**/*")
        .on('change', function(event) {
            console.log('File ' + event.path + ' was ' + event.type + ', reloading');
            browserSync.reload();
        });
});

gulp.task('default', ['work']);
